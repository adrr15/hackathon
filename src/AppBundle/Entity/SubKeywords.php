<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SubKeywords
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class SubKeywords
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="rating", type="integer")
     */
    private $rating;

    /**
     * @var Keywords
     *
     * @ORM\ManyToOne(targetEntity="Keywords", inversedBy="subKeywords")
     * @ORM\JoinColumn(name="keywords_id", referencedColumnName="id")
     */
    private $keywords;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return SubKeywords
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set rating
     *
     * @param integer $rating
     *
     * @return SubKeywords
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return integer
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @return Keywords
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * @param Keywords $keywords
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;
    }
}

