<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ParseStrategy
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class ParseStrategy
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $commentsXpath;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $commentXpath;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $usernameXpath;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $scoreXpath;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $isNotLastPageXpath;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $urlFormat;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $targetFormat;

    /**
     * @var string
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $pageLimit;




    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return ParseStrategy
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getCommentsXpath()
    {
        return $this->commentsXpath;
    }

    /**
     * @param string $commentsXpath
     * @return ParseStrategy
     */
    public function setCommentsXpath($commentsXpath)
    {
        $this->commentsXpath = $commentsXpath;

        return $this;
    }

    /**
     * @return string
     */
    public function getCommentXpath()
    {
        return $this->commentXpath;
    }

    /**
     * @param string $commentXpath
     * @return ParseStrategy
     */
    public function setCommentXpath($commentXpath)
    {
        $this->commentXpath = $commentXpath;

        return $this;
    }

    /**
     * @return string
     */
    public function getUsernameXpath()
    {
        return $this->usernameXpath;
    }

    /**
     * @param string $usernameXpath
     * @return ParseStrategy
     */
    public function setUsernameXpath($usernameXpath)
    {
        $this->usernameXpath = $usernameXpath;

        return $this;
    }

    /**
     * @return string
     */
    public function getScoreXpath()
    {
        return $this->scoreXpath;
    }

    /**
     * @param string $scoreXpath
     * @return ParseStrategy
     */
    public function setScoreXpath($scoreXpath)
    {
        $this->scoreXpath = $scoreXpath;

        return $this;
    }

    /**
     * @return string
     */
    public function getIsNotLastPageXpath()
    {
        return $this->isNotLastPageXpath;
    }

    /**
     * @param string $isNotLastPageXpath
     * @return ParseStrategy
     */
    public function setIsNotLastPageXpath($isNotLastPageXpath)
    {
        $this->isNotLastPageXpath = $isNotLastPageXpath;

        return $this;
    }

    /**
     * @return string
     */
    public function getUrlFormat()
    {
        return $this->urlFormat;
    }

    /**
     * @param string $urlFormat
     * @return ParseStrategy
     */
    public function setUrlFormat($urlFormat)
    {
        $this->urlFormat = $urlFormat;

        return $this;
    }

    /**
     * @return string
     */
    public function getTargetFormat()
    {
        return $this->targetFormat;
    }

    /**
     * @param string $targetFormat
     * @return ParseStrategy
     */
    public function setTargetFormat($targetFormat)
    {
        $this->targetFormat = $targetFormat;

        return $this;
    }

    /**
     * @return string
     */
    public function getPageLimit()
    {
        return $this->pageLimit;
    }

    /**
     * @param string $pageLimit
     * @return ParseStrategy
     */
    public function setPageLimit($pageLimit)
    {
        $this->pageLimit = $pageLimit;

        return $this;
    }



}
