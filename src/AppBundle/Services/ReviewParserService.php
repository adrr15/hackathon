<?php

namespace AppBundle\Services;

/**
 * Created by PhpStorm.
 * User: adriana.nicola
 * Date: 9/3/2016
 * Time: 2:35 PM
 */
class ReviewParserService
{
    const SERVICE_NAME = "base.review_parser";


    private $keys = array("dimension", "weight", "resolution", "screen", "displa", "memor", "camera", "sound", "video",
        "bluetooth", "color", "performance", "batter", "price", "speed");

    private $keysSentences = array();

    public function reviewParse($review)
    {
        $sentences = explode(".", $review);
        foreach ($sentences as $sentence) {
            foreach ($this->keys as $key) {
                if (strpos($sentence, $key) !== false) {
                    if (!array_key_exists($key, $this->keysSentences)) {
                        $this->keysSentences[$key] = array('positive' => array(), 'negative' => array());
                    }

                    $sentimentPercentage = json_decode(file_get_contents($this->returnURL($sentence)))->sentiment;

                    if ($sentimentPercentage < 0.4) {
                        array_push($this->keysSentences[$key]["negative"], $sentence);
                    } else if ($sentimentPercentage > 0.6) {
                        array_push($this->keysSentences[$key]["positive"], $sentence);
                    }
                }
            }
        }
    }

    private function returnURL($sentence)
    {
        return "http://apis.paralleldots.com/sentiment?sentence1=" . urlencode($sentence) . "&apikey=JAYUuMWu2422lKQdCMlq3VCIR9VsG8f6FLBn5th3sBY";
    }

    /**
     * @return array
     */
    public function getKeysSentences()
    {
        return $this->keysSentences;
    }

    /**
     * @param array $keysSentences
     */
    public function setKeysSentences($keysSentences)
    {
        $this->keysSentences = $keysSentences;
    }

}