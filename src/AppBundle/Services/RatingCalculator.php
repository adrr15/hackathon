<?php
/**
 * Created by PhpStorm.
 * User: duta.cosmin
 * Date: 9/3/2016
 * Time: 9:33 PM
 */

namespace AppBundle\Services;


use AppBundle\Entity\Keywords;

class RatingCalculator
{
    private $rating;

    private $keys = array(
        "battery"=>array("duration"=>4,"charger"=>5,"mAh"=>3,"cycle"=>4,"life"=>5),
        "camera"=>array("resolution"=>4,"color"=>3,"MP"=>5,"image"=>4,"focus"=>3),
        "memory"=>array("gygabyte"=>4,"SD"=>5,"chip"=>6,"GB"=>7),
        "price"=>array("$"=>3,"cheap"=>4,"expensive"=>5),
        "sound"=>array("audio"=>3,"hifi"=>6),
        "network"=>array("3G"=>3, "4G"=>3),
        "sensor"=>array("finger"=>6, "accelerometer"=>5, "gyro"=>4, "proximity"=>4, "compass"=>3, "barometer"=>4),
        "display"=>array("inch"=>4, "pixel"=>5, "px"=>5, "luminosity"=>4)
    );

    /**
     * @param $sentence
     */
    public function calculateRating($sentence)
    {

        $this->rating = $this->lengthRating($sentence) +
            $this->keywordsRating($sentence) +
            $this->commaRating($sentence);

    }

    /**
     * @param $sentence
     * @return float|int
     */
    function lengthRating($sentence)
    {
        $rating1 = 0;
        $sentenceLength = strlen($sentence);

        if ($sentenceLength < 35) {
            $rating1 += ($sentenceLength / 5) - 1;
        } else {
            $rating1 += 8;
        }

        return $rating1;
    }

    /**
     * @param $sentence
     * @return int
     */
    function keywordsRating($sentence)
    {
        $rating1 = 0;
        foreach ($this->getKeys() as $key => $value) {
            foreach ($value as $subKeyword => $subKeyWordValue) {
                if (strpos($sentence, $subKeyword) !== false) {
                    $rating1 += $subKeyWordValue;
                }
            }
        }
        return $rating1;
    }

    /**
     * @param $sentence
     * @return int
     */
    function commaRating($sentence)
    {
        $rating1 = 0;
        $count = substr_count($sentence, ',');

        if ($count >= 5) {
            $rating1 += 8;
        } else {
            $rating1 += $count;
        }
        return $rating1;
    }


    /**
     * @return mixed
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param mixed $rating
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
    }

    /**
     * @return array
     */
    public function getKeys()
    {
        return $this->keys;
    }

    /**
     * @param array $keys
     */
    public function setKeys($keys)
    {
        $this->keys = $keys;
    }
}


$ratingCalc = new RatingCalculator();

$sentence = "The 4 speakers work very well to balance the sound, and have the best audio range of any tablet I've ever heard, They also get very loud with no distortion";
$ratingCalc->calculateRating($sentence);
var_dump('The rating ' .$ratingCalc->getRating());