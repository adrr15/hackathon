<?php
/**
 * Created by PhpStorm.
 * User: daniel.ianosi
 * Date: 9/3/2016
 * Time: 2:08 PM
 */

namespace AppBundle\Services;


use AppBundle\Entity\ParseStrategy;
use Doctrine\ORM\EntityManager;

abstract class BaseParser
{

    /** @var EntityManager */
    protected $entityManager;

    /** @var  HTMLParser */
    protected $htmlParser;

    /** @var  ParseStrategy */
    protected $parseStrategy;

    /**
     * @param ParseStrategy $parseStrategy
     * @return array
     */
    public function getComments(ParseStrategy $parseStrategy, $startPage)
    {
        $this->setParseStrategy($parseStrategy);

        $firstUrl = $this->generateNextPage($startPage, 1);
        $this->getHtmlParser()->processURL($firstUrl);
        //die($firstUrl);
        $comments = array();
        $page = 0;
        while ($page <= $parseStrategy->getPageLimit()) {
            $page++;
            $currentUrl = $this->generateNextPage($startPage, $page);
            $this->getHtmlParser()->processURL($currentUrl);
            $pageComments = $this->getProductComments();
            foreach($pageComments as $pageComment) {
                $comments[] = $pageComment;
            }
        }

        print_r($comments);die('page $page');

        return $comments;
    }

    /**
     * array of comments with user name, comment, date and score if exists
     * @return array
     */
    abstract public function getProductComments();

    /**
     * @param int $startPage
     * @param int $page
     * @return mixed|string
     */
    public function generateNextPage($startPage, $page)
    {
        preg_match($this->getParseStrategy()->getUrlFormat(), $startPage, $matches);

        $matches['page'] = $page;

        $nextPage = $this->getParseStrategy()->getTargetFormat();
        foreach ($matches as $key => $value) {
            $nextPage= str_replace('{$'.$key.'}', $value, $nextPage);
        }

        return $nextPage;
    }

    /**
     * @return boolean
     */
    abstract public function isNotLastPage();

    /**
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * @param EntityManager $entityManager
     * @return BaseParser
     */
    public function setEntityManager($entityManager)
    {
        $this->entityManager = $entityManager;

        return $this;
    }

    /**
     * @return HTMLParser
     */
    public function getHtmlParser()
    {
        return $this->htmlParser;
    }

    /**
     * @param HTMLParser $htmlParser
     * @return BaseParser
     */
    public function setHtmlParser($htmlParser)
    {
        $this->htmlParser = $htmlParser;

        return $this;
    }

    /**
     * @return ParseStrategy
     */
    public function getParseStrategy()
    {
        return $this->parseStrategy;
    }

    /**
     * @param ParseStrategy $parseStrategy
     * @return BaseParser
     */
    public function setParseStrategy($parseStrategy)
    {
        $this->parseStrategy = $parseStrategy;

        return $this;
    }




}