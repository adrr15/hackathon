<?php
/**
 * Created by PhpStorm.
 * User: daniel.ianosi
 * Date: 6/13/15
 * Time: 3:49 PM
 */

namespace AppBundle\Services;


class HTMLParser {

    const  SERVICE_NAME = 'base.html_parser';

    /** @var string */
    public $currentHtml;

    /** @var \DOMDocument  */
    public $doc;

    public function processURL($url)
    {
        $baseUrl = str_replace('www.', '', parse_url($url, PHP_URL_HOST));

        $htmlText = file_get_contents($url);
        //$document = static::load($htmlText);
        //$normalizedHtmlText = static::serialize($document);
        $this->setCurrentHtml(strtolower($htmlText));

        libxml_use_internal_errors(true);
        $doc = new \DOMDocument();
        $convertedHtml = mb_convert_encoding($this->getCurrentHtml(), 'HTML-ENTITIES', "UTF-8");
        $doc->loadHTML('<?xml encoding="UTF-8">' . $convertedHtml);
        $this->setDoc($doc);
    }

    /**
     * @param string   $xPath
     * @param \DOMNode $node
     * @return \DOMNodeList
     */
    public function getNodeByXpath($xPath, \DOMNode $node = null)
    {
        $html = new \DOMXPath($this->getDoc());

        return $html->query($xPath, $node);
    }

    public static function verifyIfNodeIsChild(\DOMNode $parentNode, \DOMNode $childNode)
    {
        while ($childNode->parentNode) {
            $childNode = $childNode->parentNode;
            if ($childNode == $parentNode) {
                return true;
            }
        }
        return false;
    }

    public static function getHTMLFromNodeToNode($doc, \DOMNode $startNode, \DOMNode $endNode)
    {
        $resultHTML = '';
        while ($startNode->nextSibling) {
            $startNode = $startNode->nextSibling;
            $resultHTML.= $doc->saveHTML($startNode);
        }
        if (!self::verifyIfNodeIsChild($startNode, $endNode)) {
            echo "1";
            while ($startNode->parentNode){
                $startNode = $startNode->parentNode;
                echo "2";
                if ($startNode->nextSibling)
                {
                    echo "3";
                    break;
                }
            }
            if ($startNode) {
                echo "4";
                return $resultHTML . self::getHTMLFromNodeToNode($doc, $startNode, $endNode);
            } else {
                echo "5";
                return $resultHTML;
            }
        }
    }

    public static function findParentNodeThatContainsText($doc, \DOMNode $node, $text)
    {
        if (strpos(strip_tags($doc->saveHTML($node)), $text)) {
            return $node;
        }
        while ($node->parentNode) {
            if (strpos(strip_tags($doc->saveHTML($node)), $text)) {
                return $node;
            } else {
                $node = $node->parentNode;
            }
        }

        return null;
    }



    static public function findNearest($subject, $mark, array $values = array(), $findNumbers = true)
    {
        $subject = preg_replace('/[^a-z0-9]+/i', ' ',$subject);
        $lastPreMatchPosition = false;
        $firstPostMatchPosition = false;
        $patterns = $values;
        if ($findNumbers) {
            $patterns[] = "\d*\.?\d+?";
        }

        $patterns_flattened = str_replace('/', '\/', implode('|', $patterns));

        $preString = substr($subject, 0, strpos($subject, $mark));
        $postString = substr($subject, strpos($subject, $mark)+1, strlen($subject)-1);

        if ( preg_match_all('/'. $patterns_flattened .'/', $preString, $preMatches) )
        {
            $lastPreMatch =  array_pop($preMatches[0]);
            $lastPreMatchPosition = strrpos($preString, $lastPreMatch);
        }

        if ( preg_match_all('/'. $patterns_flattened .'/', $postString, $postMatches) )
        {
            $firstPostMatch =  array_shift($postMatches[0]);
            $firstPostMatchPosition = strpos($postString, $firstPostMatch);
        }

        if ($lastPreMatchPosition !== false){
            $preLength = strpos($subject, $mark) - $lastPreMatchPosition + strlen($lastPreMatch);
        } else {
            $preLength = 1000000;
        }



        if ($firstPostMatchPosition !== false){
            $postLength = $firstPostMatchPosition  + strlen($mark);
        } else {
            $postLength = 1000000;
        }



        if ($preLength ==  1000000 && $postLength == 1000000) { return null; }

        if ($preLength > $postLength) {
            return $firstPostMatch;
        } else {
            return $lastPreMatch;
        }
    }

    /**
     * @param mixed $currentHtml
     */
    public function setCurrentHtml($currentHtml)
    {
        $this->currentHtml = $currentHtml;
    }

    /**
     * @return mixed
     */
    public function getCurrentHtml()
    {
        return $this->currentHtml;
    }

    /**
     * @param \DOMDocument $doc
     */
    public function setDoc($doc)
    {
        $this->doc = $doc;
    }

    /**
     * @return \DOMDocument
     */
    public function getDoc()
    {
        return $this->doc;
    }
} 