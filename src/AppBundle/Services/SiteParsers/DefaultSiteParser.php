<?php
namespace AppBundle\Services\SiteParsers;
use AppBundle\Services\BaseParser;

/**
 * Created by PhpStorm.
 * User: daniel.ianosi
 * Date: 9/3/2016
 * Time: 3:03 PM
 */
class DefaultSiteParser extends BaseParser
{

    const  SERVICE_NAME = 'default.site.parser';

    /**
     * array of comments with user name, comment and score if exists
     * @return array
     */
    public function getProductComments()
    {
        $comments = array();
        $commentsList = $this->getHtmlParser()->getNodeByXpath($this->getParseStrategy()->getCommentsXpath());
        foreach ($commentsList as $commentItem) {
            $commentNode = $this->getHtmlParser()->getNodeByXpath($this->getParseStrategy()->getCommentXpath(), $commentItem);
            $commentText =  $commentNode->item(0)->textContent;
            $userText = '';
            if ($this->getParseStrategy()->getUsernameXpath()) {
                $userNode = $this->getHtmlParser()->getNodeByXpath($this->getParseStrategy()->getUsernameXpath(), $commentItem);
                if ($userNode->length > 0) {
                    $userText = $userNode->item(0)->textContent;
                }
            }
            $scoreText = '';
            if ($this->getParseStrategy()->getScoreXpath()) {
                $scoreNode = $this->getHtmlParser()->getNodeByXpath($this->getParseStrategy()->getScoreXpath(), $commentItem);
                if ($scoreNode->length > 0) {
                    $scoreText = $scoreNode->item(0)->textContent;
                }
            }

            $comments[] = array(
                'comment' => $commentText,
                'user'    => $userText,
                'score'   => $scoreText,
            );
        }

        return $comments;
    }

    /**
     * @return boolean
     */
    public function isNotLastPage()
    {
        return true;
        $notLastPageNode = $this->getHtmlParser()->getNodeByXpath($this->getParseStrategy()->getIsNotLastPageXpath());

        print_r($notLastPageNode);die;
        return ($notLastPageNode->length>0);
    }
}