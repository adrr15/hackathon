<?php

namespace AppBundle\Command;

use AppBundle\Services\SiteParsers\DefaultSiteParser;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @codeCoverageIgnore
 *
 * Generate iServices and all dependencies and publish them for IsvcCacheObject
 *
 * Run command: php app/console isvc.definitions.generate_extrawarranties
 *
 * Class GenerateExtraWarrantiesDefinitions
 *
 * @package Emag\IsvcBundle\Command
 */
class TestParserCommand extends ContainerAwareCommand
{

    /**
     * Configurartion of command
     */
    protected function configure()
    {
        $this->setName('test:parser')
            ->setDescription('Test parser');
    }

    /**
     * Execution of command
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var DefaultSiteParser $defaultSiteParser*/
        $defaultSiteParser = $this->getContainer()->get(DefaultSiteParser::SERVICE_NAME);
        /** @var EntityManager $entityManager */
        $entityManager = $this->getContainer()->get('doctrine.orm.entity_manager');

        $parseStrategy = $entityManager->getRepository('AppBundle:ParseStrategy')->find(1);
        $defaultSiteParser->getComments($parseStrategy, 'http://www.gsmarena.com/apple_iphone_6s-7242.php');



    }
}