<?php

namespace AppBundle\Command;

use AppBundle\Services\ReviewParserService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Created by PhpStorm.
 * User: adriana.nicola
 * Date: 9/4/2016
 * Time: 3:33 AM
 */
class ReviewParserCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('review:kk');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Started...');

        $service = $this->getContainer()->get(ReviewParserService::SERVICE_NAME);
        $service->reviewParse("The battery sucks. It has a nice camera. It is poo. The display has a nice color.");
        print_r($service->getKeysSentences());

        $output->writeln('Ended!');
    }
}